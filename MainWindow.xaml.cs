﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oefening_32._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //List<Brush> colors = new List<Brush>();
        //List<SolidColorBrush> colors2 = new List<SolidColorBrush>();
        //List<Color> colors3 = new List<Color>();
        List<String> strColors = new List<String>();

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Brush brush = Brushes.Blue;
            //Brush brush1 = Brushes.Green;
            //Brush brush2 = Brushes.Red;

            //colors.Add(brush);
            //colors.Add(brush1);
            //colors.Add(brush2);

            //BrushConverter conv = new BrushConverter();

            //SolidColorBrush blue = conv.ConvertFromString("Blue") as SolidColorBrush;
            //SolidColorBrush red = conv.ConvertFromString("Red") as SolidColorBrush;
            //SolidColorBrush green = conv.ConvertFromString("Green") as SolidColorBrush;

            //colors2.Add(blue);
            //colors2.Add(red);
            //colors2.Add(green);

            strColors.Add("Blue");
            strColors.Add("Red");
            strColors.Add("Green");

            cmbKleurBinnenkant.ItemsSource = strColors;
            cmbKleurBuitenkant.ItemsSource = strColors;
            cmbKleurBuitenkant.SelectedIndex = 0;
            cmbKleurBinnenkant.SelectedIndex = 0;
        }
    }
}
